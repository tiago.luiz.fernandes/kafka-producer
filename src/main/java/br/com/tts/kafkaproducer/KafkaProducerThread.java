package br.com.tts.kafkaproducer;


import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class KafkaProducerThread extends Thread {
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducerThread.class);
	
	private String name;
	private Properties properties;
	private String topic;
		
	public KafkaProducerThread(String name, String topic, Properties properties) {
		this.name = name;
		this.topic = topic;
		this.properties = properties;
	}
	
	@Override
	public void run() {
		int endExecution = 0;
				
		logger.info(name + " begin");
		
		while(endExecution == 0) {
			logger.info(name + " running");
			
			Properties execProperties = new Properties();
			try {
				execProperties.load(new FileInputStream("properties"));
				endExecution = Integer.valueOf(execProperties.getProperty("producer.endExecution"));
			} catch (Exception e) {
				endExecution = 1;
				logger.info(name + " error");
				e.printStackTrace();
			}
		    		    	    		
												
			Producer<String, String> producer = new KafkaProducer<>(properties);			
	        		        
	        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	        	        	
        	JSONObject jsonObject = new JSONObject();
        	Date data = new Date();
             
        	jsonObject.put("producerName", name);
        	jsonObject.put("topic", topic);	        	 
        	jsonObject.put("date", sdf.format(data));
        	 
        	logger.info(name + " sending");
        	producer.send(new ProducerRecord<>(topic, "jason_data", jsonObject.toString()) ) ;
	        
	
	        producer.close();
	        
	        try {
				sleep(5000);
			} catch (InterruptedException e) {
				endExecution = 1;				
				logger.info(name + " error");
				e.printStackTrace();
			}
		}    
                
		logger.info(name + " end");
	
	}

}