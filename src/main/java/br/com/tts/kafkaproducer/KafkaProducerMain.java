package br.com.tts.kafkaproducer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;


public class KafkaProducerMain{
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducerMain.class);

    public static void main(String[] args){
    	
    	    	
    	logger.info("begin");
    	
    	Properties properties = new Properties();
    	properties.put("bootstrap.servers", "localhost:9092");
    	properties.put("acks", "all");
    	properties.put("retries", 0);
    	properties.put("batch.size", 16384);
    	properties.put("linger.ms", 1);
    	properties.put("buffer.memory", 33554432);
    	properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    	properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        
        KafkaProducerThread kpt1 = new KafkaProducerThread("KP1","TOPIC1",properties);
        //KafkaProducerThread kpt2 = new KafkaProducerThread("KP2","TOPIC2",properties);
        //KafkaProducerThread kpt3 = new KafkaProducerThread("KP3","TOPIC3",properties);        
        //KafkaProducerThread kpt4 = new KafkaProducerThread("KP4","TOPIC4",properties);
        
        kpt1.start();
        //kpt2.start();
        //kpt3.start();
        //kpt4.start();
                              
        logger.info("end");

    }

}
